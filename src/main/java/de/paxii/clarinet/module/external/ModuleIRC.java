package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerSendChatMessageEvent;
import de.paxii.clarinet.irc.IRCManager;
import de.paxii.clarinet.irc.chat.Chat;
import de.paxii.clarinet.irc.chat.PrefixManager;
import de.paxii.clarinet.irc.util.data.PrefixList;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.settings.type.ClientSettingString;
import de.paxii.clarinet.util.web.JsonFetcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 19.09.2015.
 */
public class ModuleIRC extends Module {
  @Getter
  private static ModuleIRC INSTANCE;
  @Getter
  private PrefixManager prefixManager;
  @Getter
  private IRCManager ircManager;
  @Getter
  private ArrayList<String> ignoredPlayers;
  @Getter
  @Setter
  private String lastSender = "";
  @Getter
  @Setter
  private boolean joinMessages;
  @Getter
  @Setter
  private boolean chatToggle;

  // TODO: Use Client::clientURL instead of this
  private String prefixURL = "http://matix.paxii.de/api/irc";

  public ModuleIRC() {
    super("IRC", ModuleCategory.WORLD);

    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setVersion("1.1.1");
    this.setBuildVersion(16501);
    this.setSyntax("irc <connect/disconnect/joinmessage/chattoggle/prefix/nick>");
    this.setDescription("IRC Settings");

    this.ignoredPlayers = new ArrayList<>();
    this.prefixManager = new PrefixManager(new HashMap<>());
    this.loadPrefixes();
  }

  @Override
  public void onStartup() {
    ModuleIRC.INSTANCE = this;
    this.ircManager = new IRCManager();
    this.setEnabled(true);

    if (this.getModuleSettings().containsKey("ignoredPlayers")) {
      String[] ignoredPlayers = this.getValueOrDefault("ignoredPlayers", String.class, "").split(",");

      this.ignoredPlayers.addAll(Arrays.asList(ignoredPlayers));
    }
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
    this.ircManager.startBot();
  }

  private void loadPrefixes() {
    new Thread(() -> {
      PrefixList prefixList = JsonFetcher.get(this.prefixURL, PrefixList.class);
      ModuleIRC.this.getPrefixManager().setPrefixMap(prefixList.getTags());
    }).start();
  }

  @EventHandler
  public void onSendMessage(PlayerSendChatMessageEvent event) {
    String ircPrefix = this.getValueOrDefault("prefix", String.class, "@");

    if (this.chatToggle) {
      if (!event.getChatMessage().startsWith(ircPrefix)) {
        event.setChatMessage(ircPrefix + event.getChatMessage());
      }
    }

    if (!event.getChatMessage().startsWith(ircPrefix)) {
      return;
    }

    event.setCancelled(true);

    if (!this.ircManager.getPircBotX().isConnected()) {
      Chat.printIRCMessage("You are not connected to the IRC!");
      return;
    }

    if (event.getChatMessage().startsWith(ircPrefix + "/msg")) {
      String line = event.getChatMessage();

      if (line.contains(" ")) {
        String[] split = line.split(" ");

        if (split.length >= 3) {
          String receiver = split[1];
          String message = "";

          for (int i = 2; i < split.length; i++)
            message += split[i] + " ";

          message = message.trim();

          this.ircManager.getPircBotX().sendIRC().message(receiver, message);
          if (this.getIrcManager().getIrcUtils().isUserSpecial(this.getIrcManager().getPircBotX().getUserBot())) {
            message = Chat.replaceColors(message);
          }
          Chat.printIRCMessage("You > " + this.prefixManager.getPrefix(receiver) + ": " + message);
        }
      } else {
        Chat.printIRCMessage(String.format("Syntax Error: '%s/msg <username> <message>'", ircPrefix));
      }
    } else if (event.getChatMessage().startsWith(ircPrefix + "/r")) {
      if (this.getLastSender().length() > 0) {
        if (event.getChatMessage().length() > ircPrefix.length() + 3) {
          String message = event.getChatMessage().substring(ircPrefix.length() + 3);

          this.ircManager.getPircBotX().sendIRC().message(this.getLastSender(), message);
          if (this.getIrcManager().getIrcUtils().isUserSpecial(this.getIrcManager().getPircBotX().getUserBot())) {
            message = Chat.replaceColors(message);
          }
          Chat.printIRCMessage("You > " + this.prefixManager.getPrefix(this.getLastSender()) + ": " + message);
        } else {
          Chat.printIRCMessage("Error: No Message supplied!");
        }
      } else {
        Chat.printIRCMessage("Nobody to reply to!");
      }
    } else {
      String ircMessage = event.getChatMessage().substring(ircPrefix.length());

      if (ircMessage.length() > 0) {
        this.ircManager.getPircBotX().sendIRC().message(this.ircManager.getIrcConfiguration().getChannelName(), ircMessage);
        if (this.getIrcManager().getIrcUtils().isUserSpecial(this.getIrcManager().getPircBotX().getUserBot())) {
          ircMessage = Chat.replaceColors(ircMessage);
        }
        Chat.printIRCMessage(this.prefixManager.getPrefix(this.ircManager.getPircBotX().getNick()) + ": " + ircMessage);
      } else {
        Chat.printIRCMessage("Error: No Message supplied!");
      }
    }
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      String cmd = args[0];

      if (cmd.equalsIgnoreCase("connect")) {
        if (!this.ircManager.getPircBotX().isConnected()) {
          this.ircManager.startBot();
        } else {
          Chat.printIRCMessage("Already connected!");
        }
      } else if (cmd.equalsIgnoreCase("disconnect")) {
        if (this.ircManager.getPircBotX().isConnected()) {
          this.ircManager.stopBot();
        }
      } else if (cmd.equalsIgnoreCase("joinmessage")) {
        this.joinMessages = !this.joinMessages;

        Chat.printIRCMessage(String.format("IRC Join Messages have been %s!", this.joinMessages ? "enabled" : "disabled"));
      } else if (cmd.equalsIgnoreCase("chattoggle")) {
        this.chatToggle = !this.chatToggle;

        Chat.printIRCMessage(String.format("IRC Chat has been %s!", this.chatToggle ? "enabled" : "disabled"));
      } else if (cmd.equalsIgnoreCase("ignore")) {
        if (args.length >= 2) {
          if (args[1].equalsIgnoreCase("list")) {
            Chat.printIRCMessage("Ignored Players: " + String.join(", ", this.ignoredPlayers));
          } else {
            if (this.ignoredPlayers.contains(args[1])) {
              this.ignoredPlayers.remove(args[1]);

              Chat.printIRCMessage("Player " + args[1] + " is no longer ignored!");
            } else {
              this.ignoredPlayers.add(args[1]);

              Chat.printIRCMessage("Player " + args[1] + " is now ignored!");
            }
          }
        } else {
          Chat.printIRCMessage("Too few arguments!");
        }
      } else if (cmd.equalsIgnoreCase("prefix")) {
        if (args.length >= 2) {
          String prefix = args[1];

          this.getModuleSettings().put("prefix", new ClientSettingString("prefix", prefix));
          Chat.printIRCMessage(String.format("IRC Prefix has been to '%s'.", prefix));
        } else {
          Chat.printIRCMessage("Too few arguments!");
        }
      } else if (cmd.equalsIgnoreCase("nick")) {
        if (args.length == 2) {
          String nickName = args[1];

          if (!this.getIrcManager().getPircBotX().getUserChannelDao().containsUser(nickName)) {
            this.getIrcManager().getPircBotX().sendIRC().changeNick(nickName);
            Chat.printIRCMessage(String.format("You changed your nick to %s.", nickName));
          } else {
            Chat.printIRCMessage("A user with that nick is already connected.");
          }
        } else {
          Chat.printIRCMessage("Invalid usage! (irc nick <username>");
        }
      } else {
        Chat.printIRCMessage("Unknown subcommand!");
      }
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);

    this.ircManager.stopBot();
  }

  @Override
  public void onShutdown() {
    this.getModuleSettings().put("ignoredPlayers", new ClientSettingString("ignoredPlayers", String.join(",", this.ignoredPlayers)));
  }
}
