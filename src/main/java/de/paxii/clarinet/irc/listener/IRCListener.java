package de.paxii.clarinet.irc.listener;

import de.paxii.clarinet.irc.chat.Chat;
import de.paxii.clarinet.module.external.ModuleIRC;
import de.paxii.clarinet.util.chat.ChatColor;

import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ConnectEvent;
import org.pircbotx.hooks.events.DisconnectEvent;
import org.pircbotx.hooks.events.JoinEvent;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.NickAlreadyInUseEvent;
import org.pircbotx.hooks.events.NoticeEvent;
import org.pircbotx.hooks.events.PartEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

/**
 * Created by Lars on 19.09.2015.
 */
public class IRCListener extends ListenerAdapter {

  @Override
  public void onConnect(ConnectEvent event) {
    Chat.printIRCMessage("Connected to IRC!");
  }

  @Override
  public void onJoin(JoinEvent event) {
    String userName = event.getUser().getNick();

    if (userName.equals(event.getBot().getNick())) {
      Chat.printIRCMessage("You joined " + event.getChannel().getName());
    } else {
      if (ModuleIRC.getINSTANCE().isJoinMessages()) {
        userName = ModuleIRC.getINSTANCE().getPrefixManager().getPrefix(userName);

        Chat.printIRCMessage(userName + " joined " + event.getChannel().getName());
      }
    }
  }

  @Override
  public void onDisconnect(DisconnectEvent event) {
    Chat.printIRCMessage("Disconnected from IRC!");
  }

  @Override
  public void onPart(PartEvent event) {
    String userName = event.getUser().getNick();

    if (userName.equals(event.getBot().getNick())) {
      Chat.printIRCMessage("You left " + event.getChannel().getName());
    } else {
      if (ModuleIRC.getINSTANCE().isJoinMessages()) {
        userName = ModuleIRC.getINSTANCE().getPrefixManager().getPrefix(userName);

        Chat.printIRCMessage(userName + " left " + event.getChannel().getName());
      }
    }
  }

  @Override
  public void onMessage(MessageEvent event) {
    String message = event.getMessage().replaceAll("§", "");
    String userName = event.getUser().getNick();

    if (!ModuleIRC.getINSTANCE().getIgnoredPlayers().contains(userName)) {
      userName = ModuleIRC.getINSTANCE().getPrefixManager().getPrefix(userName);
      if (ModuleIRC.getINSTANCE().getIrcManager().getIrcUtils().isUserSpecial(event.getUser(), event.getChannel())) {
        message = Chat.replaceColors(message);
      }
      Chat.printIRCMessage(userName + ": " + message);
    }
  }

  @Override
  public void onPrivateMessage(PrivateMessageEvent event) {
    String message = event.getMessage().replaceAll("§", "");
    String userName = event.getUser().getNick();
    ModuleIRC.getINSTANCE().setLastSender(userName);

    if (!ModuleIRC.getINSTANCE().getIgnoredPlayers().contains(userName)) {
      userName = ModuleIRC.getINSTANCE().getPrefixManager().getPrefix(userName);
      if (ModuleIRC.getINSTANCE().getIrcManager().getIrcUtils().isUserSpecial(event.getUser())) {
        message = Chat.replaceColors(message);
      }
      Chat.printIRCMessage(userName + " > You: " + message);
    }
  }

  @Override
  public void onNotice(NoticeEvent event) {
    String message = event.getMessage().replaceAll("§", "");
    String userName = event.getUser().getNick();

    if (!(userName.equals("NickServ") || userName.equals("ChanServ") || userName.equals("OperServ"))) {
      return;
    }

    userName = ModuleIRC.getINSTANCE().getPrefixManager().getPrefix(userName);

    Chat.printIRCMessage(ChatColor.GREEN + "[NOTICE] " + ChatColor.RESET + userName + " > You: " + message);
  }

  @Override
  public void onNickAlreadyInUse(NickAlreadyInUseEvent event) {
    String autoNick = event.getAutoNewNick();
    Chat.printIRCMessage("Your Username is already in use! Changing nick to " + autoNick + ".");
    event.respond(autoNick);
  }
}
