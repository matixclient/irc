package de.paxii.clarinet.irc.util;

import com.google.common.collect.ImmutableSortedSet;

import de.paxii.clarinet.module.external.ModuleIRC;

import org.pircbotx.Channel;
import org.pircbotx.User;

/**
 * Created by Lars on 05.06.2016.
 */
public class IRCUtils {

  public boolean isUserAdmin(User user, Channel channel) {
    return channel.getOps().contains(user) ||
            channel.getSuperOps().contains(user) ||
            channel.getOwners().contains(user) ||
            channel.getHalfOps().contains(user);
  }

  public boolean isUserSpecial(User user, Channel channel) {
    return this.isUserAdmin(user, channel) || channel.getVoices().contains(user);
  }

  public boolean isUserAdmin(User user) {
    ImmutableSortedSet<Channel> channelList = ModuleIRC.getINSTANCE().getIrcManager().getPircBotX().getUserChannelDao().getChannels(ModuleIRC.getINSTANCE().getIrcManager().getPircBotX().getUserBot());

    for (Channel channel : channelList) {
      if (this.isUserAdmin(user, channel))
        return true;
    }

    return false;
  }

  public boolean isUserSpecial(User user) {
    ImmutableSortedSet<Channel> channelList = ModuleIRC.getINSTANCE().getIrcManager().getPircBotX().getUserChannelDao().getChannels(ModuleIRC.getINSTANCE().getIrcManager().getPircBotX().getUserBot());

    for (Channel channel : channelList) {
      if (this.isUserSpecial(user, channel))
        return true;
    }

    return false;
  }
}
