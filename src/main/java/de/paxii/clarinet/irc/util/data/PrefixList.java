package de.paxii.clarinet.irc.util.data;

import java.util.HashMap;

import lombok.Data;

/**
 * Created by Lars on 01.10.2016.
 */
@Data
public class PrefixList {
  private HashMap<String, String> tags;

  public PrefixList() {
    this.tags = new HashMap<>();
  }
}
