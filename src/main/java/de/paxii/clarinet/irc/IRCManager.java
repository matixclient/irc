package de.paxii.clarinet.irc;

import com.google.gson.Gson;

import de.paxii.clarinet.Client;
import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.irc.configuration.IRCConfiguration;
import de.paxii.clarinet.irc.listener.IRCListener;
import de.paxii.clarinet.irc.util.IRCUtils;
import de.paxii.clarinet.module.external.ModuleIRC;
import de.paxii.clarinet.util.settings.ClientSettings;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Scanner;

import lombok.Getter;

/**
 * Created by Lars on 19.09.2015.
 */
public class IRCManager {
  @Getter
  private PircBotX pircBotX;
  @Getter
  private IRCConfiguration ircConfiguration;
  @Getter
  private IRCUtils ircUtils;
  private Configuration configuration;

  private Thread botThread;

  public IRCManager() {
    File ircFolder = new File(ClientSettings.getClientFolderPath().getValue() + "/irc/");
    File ircFile = new File(ircFolder, "IRC.json");
    this.ircUtils = new IRCUtils();

    ircFolder.mkdirs();

    if (!ircFile.exists()) {
      try {
        ircFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
      try {
        PrintWriter pw = new PrintWriter(ircFile);

        pw.println("{");
        pw.println("\t\"userName\":\"" + Wrapper.getMinecraft().getSession().getUsername() + "\",");
        pw.println("\t\"nickServPassword\":\"NULL\",");
        pw.println("\t\"hostName\":\"irc.paxii.de\",");
        pw.println("\t\"channelName\":\"#Matix\",");
        pw.println("\t\"serverPassword\":\"Fitudu13\",");
        pw.println("\t\"serverPort\":" + 6667);
        pw.println("}");

        pw.close();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
    }

    if (ircFile.exists()) {
      String jsonString = "";

      try {
        Scanner sc = new Scanner(ircFile);

        while (sc.hasNextLine()) {
          jsonString += sc.nextLine();
        }

        sc.close();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }

      if (jsonString.length() > 0) {
        Gson gson = new Gson();

        this.ircConfiguration = gson.fromJson(jsonString, IRCConfiguration.class);
      }
    }

    if (this.ircConfiguration != null) {
      this.configuration = new Configuration.
              Builder().
              setName(this.ircConfiguration.getUserName()).
              setRealName(String.format("%s (%s) v%s b%d IRC v%s",
                      Client.getClientName(),
                      Client.getGameVersion(),
                      Client.getClientVersion(),
                      Client.getClientBuild(),
                      ModuleIRC.getINSTANCE().getVersion())
              ).
              setLogin(this.ircConfiguration.getUserName()).
              setServerHostname(this.ircConfiguration.getHostName()).
              setServerPassword(this.ircConfiguration.getServerPassword()).
              setServerPort(this.ircConfiguration.getServerPort()).
              addAutoJoinChannel(this.ircConfiguration.getChannelName()).
              setNickservPassword(this.ircConfiguration.getNickServPassword()).
              setEncoding(Charset.forName("UTF-8")).
              setAutoNickChange(true).
              addListener(new IRCListener())
              .buildConfiguration();
    }

    this.pircBotX = new PircBotX(this.configuration);
  }

  public void startBot() {
    this.botThread = new Thread(() -> {
      try {
        IRCManager.this.pircBotX.startBot();
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
    botThread.start();
  }

  public void stopBot() {
    this.pircBotX.sendIRC().quitServer("Disabling plugin");
  }
}
