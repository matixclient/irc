package de.paxii.clarinet.irc.chat;

import de.paxii.clarinet.util.chat.ChatColor;

/**
 * Created by Lars on 19.09.2015.
 */
public class Chat {
  private static String prefix = ChatColor.GREEN + "[IRC] " + ChatColor.RESET;

  public static void printIRCMessage(String message) {
    de.paxii.clarinet.util.chat.Chat.printChatMessage(prefix + message);
  }

  /**
   * Replaces &-color codes while preserving URLs
   *
   * @return String
   */
  public static String replaceColors(String message) {
    StringBuilder stringBuilder = new StringBuilder();

    if (message.contains(" ")) {
      String[] messageParts = message.split(" ");

      for (String messagePart : messageParts) {
        if (!(messagePart.startsWith("http://") || messagePart.startsWith("https://"))) {
          messagePart = ChatColor.translateAlternateColorCodes('&', messagePart);
        }
        stringBuilder.append(messagePart).append(" ");
      }
    } else if (!(message.startsWith("http://") || message.startsWith("https://"))) {
      stringBuilder.append(ChatColor.translateAlternateColorCodes('&', message));
    }

    return stringBuilder.toString().trim();
  }
}
