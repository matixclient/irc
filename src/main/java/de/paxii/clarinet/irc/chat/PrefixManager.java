package de.paxii.clarinet.irc.chat;

import de.paxii.clarinet.util.chat.ChatColor;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 19.09.2015.
 */
public class PrefixManager {
  @Getter
  @Setter
  private HashMap<String, String> prefixMap;

  public PrefixManager(HashMap<String, String> prefixMap) {
    this.prefixMap = prefixMap;
  }

  public String getPrefix(String userName) {
    if (userName == null) {
      userName = "You";
    } else {
      if (this.prefixMap.containsKey(userName)) {
        return ChatColor.translateAlternateColorCodes('&', this.prefixMap.get(userName) + " " + userName + ChatColor.RESET);
      }
    }

    return userName;
  }
}
