package de.paxii.clarinet.irc.configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 19.09.2015.
 */
public class IRCConfiguration {

  @Getter
  @Setter
  private String userName;
  @Getter
  @Setter
  private String nickServPassword;
  @Getter
  @Setter
  private String hostName;
  @Getter
  @Setter
  private String channelName;
  @Getter
  @Setter
  private String serverPassword;
  @Getter
  @Setter
  private int serverPort;

  public IRCConfiguration(String userName, String nickServPassword, String hostName, String channelName, String serverPassword, int serverPort) {
    this.userName = userName;
    this.nickServPassword = nickServPassword;
    this.hostName = hostName;
    this.channelName = channelName;
    this.serverPassword = serverPassword;
    this.serverPort = serverPort;
  }
}
